<?php

namespace AppBundle\Controller;


use Goutte\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Service\ProductManager;




class FrontController extends Controller
{
    /**
     * @Route("/", name="catalog")
     */
    public function catalogAction(Request $request, ProductManager $productManager)
    {
        $products = $productManager->getProducts();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $products,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('@App/Front/catalog.html.twig', [
            'products' => $products,
            'pagination' => $pagination
        ]);
    }


}
