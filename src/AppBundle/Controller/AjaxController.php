<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Entity\Wishlist;
use AppBundle\Service\WishlistManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/ajax")
 */
class AjaxController extends Controller
{

    protected $wishlistManager;

    public function __construct(WishlistManager $wishlistManager)
    {
        $this->wishlistManager = $wishlistManager;
    }

    /**
     * @Route("/wishlist/{wishlist}/product/{product}", name="ajax_add_wishlist_product", methods={"POST"})
     *
     */
    public function postWishlistProductAction(Request $request, Wishlist $wishlist, Product $product)
    {

        $response = $this->wishlistManager->addProductToWishlist($wishlist,  $product);

        return new JsonResponse($response['body'],$response['code']);
    }

    /**
     * @Route("/wishlist/{wishlist}/product/{product}", name="ajax_delete_wishlist_product", methods={"DELETE"})
     *
     */
    public function deleteWishlistProductAction(Request $request, Wishlist $wishlist, Product $product)
    {
        $response = $this->wishlistManager->removeProductFromWishlist($wishlist,  $product);

        return new JsonResponse($response['body'],$response['code']);

    }

}