<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/friend")
 */
class FriendController extends Controller
{

    /**
     * @Route("/{friend}/wishlist", name="friend_wishlist")
     */
    public function showWishlistAction(Request $request, $friend)
    {
        $wishlist = null;
        $errors = [];
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $friend = $em->getRepository('AppBundle\Entity\User')->find($friend);

        // check if these users have some friendship.
        $friends = $em->getRepository('AppBundle\Entity\Friends')->findOneBy(array(
            'userA' => [$user, $friend],
            'userB' => [$user, $friend]
            ));

        if ($friend){
            if (!$friends){
                $errors[] = 'Acces denied. You have to be friends to share wishlists.';
            }else{
                $wishlist = $friend->getWishlist();
            }
        }else
            $errors[] = 'User doesnt exist.';

        return $this->render('@App/Back/friends_wishlist.html.twig',[
            'wishlist' => $wishlist,
            'errors' => $errors
        ]);
    }


    // TODO : Actions for making friends and rules like the user cant request friendship with himself

}
