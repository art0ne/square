<?php
namespace AppBundle\Command;

use AppBundle\Crawler\CrawlerApi;
use AppBundle\Crawler\CrawlerWeb;
use AppBundle\Crawler\Updater;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class UpdateDatabaseCommand extends ContainerAwareCommand
{


    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:update-db')

            ->addArgument('src', InputArgument::REQUIRED, 'The source of data : api | web.')

            // the short description shown while running "php bin/console list"
            ->setDescription('Updates the product database.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command updates the DB with products from different sources. Example: app:update-db web')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $src = $input->getArgument('src');
        if ($src == 'web')
            $adapter = new CrawlerWeb();
        //elseif ($src == 'api')
            //$adapter = new CrawlerApi();

        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        $normalizer = new ObjectNormalizer();
        $serializer = new Serializer(array($normalizer));

        $updater = new Updater($adapter, $em, $serializer);
        $updater->execute();

        error_log("DONE!");
    }
}
